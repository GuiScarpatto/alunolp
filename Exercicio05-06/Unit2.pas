unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm2 = class(TForm)
    Menu1: TMemo;
    Bt_tamanho: TButton;
    Menu2: TMemo;
    Bt_vf: TButton;
    Bt_s_espaco: TButton;
    Bt_minusculo: TButton;
    Bt_maiusculo: TButton;
    bt_replace: TButton;
    procedure Bt_tamanhoClick(Sender: TObject);
    procedure Bt_s_espacoClick(Sender: TObject);
    procedure Bt_minusculoClick(Sender: TObject);
    procedure Bt_maiusculoClick(Sender: TObject);
    procedure bt_replaceClick(Sender: TObject);
    procedure Bt_vfClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.Bt_tamanhoClick(Sender: TObject);
var t : string;
begin
 t := menu1.text;
 menu2.text :=(t.Length.ToString)
end;

procedure TForm2.Bt_vfClick(Sender: TObject);
var t : string;
begin
t:=menu1.Text;
menu2.text := BoolToStr( t.Contains('m'));

end;

procedure TForm2.Bt_s_espacoClick(Sender: TObject);
var t : string ;
begin
t:= menu1.Text;
menu2.Text := t.Trim;
end;

procedure TForm2.Bt_minusculoClick(Sender: TObject);
var t : string ;
begin
t:= menu1.Text ;
 menu2.Text := t.LowerCase(t) ;

end;

procedure TForm2.Bt_maiusculoClick(Sender: TObject);
var t : string ;
begin
t:= menu1.Text ;
 menu2.Text := t.UpperCase(t) ;
end;
procedure TForm2.bt_replaceClick(Sender: TObject);
var t : string ;

begin
t:= menu1.Text ;
menu2.Text := t.Replace('u','t');

end;

end.
